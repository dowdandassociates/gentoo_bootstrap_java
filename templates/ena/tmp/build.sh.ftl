#!/bin/bash

echo "emerge-webrsync"
emerge-webrsync

echo "Setup files"

mkdir -p /boot/grub

<#assign filename = "/boot/grub/menu.lst">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/boot/grub/menu.lst.ftl">
EOF

mkdir -p /etc

<#assign filename = "/etc/fstab">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/etc/fstab.ftl">
EOF

mkdir -p /etc/local.d

<#assign filename = "/etc/local.d/public-keys.start">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/etc/local.d/public-keys.start.ftl">
EOF
chmod 755 ${filename}

<#assign filename = "/etc/local.d/public-keys.stop">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/etc/local.d/public-keys.stop.ftl">
EOF
chmod 755 ${filename}

<#assign filename = "/etc/local.d/killall_nash-hotplug.start">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/etc/local.d/killall_nash-hotplug.start.ftl">
EOF
chmod 755 ${filename}

<#assign filename = "/etc/local.d/makeopts.start">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/etc/local.d/makeopts.start.ftl">
EOF
chmod 755 ${filename}

<#assign filename = "/etc/local.d/fix-dev.start">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/etc/local.d/fix-dev.start.ftl">
EOF
chmod 755 ${filename}

echo "/etc/localtime"
cp -L /usr/share/zoneinfo/UTC /etc/localtime

mkdir -p /etc/portage

<#assign filename = "/etc/portage/make.conf">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/etc/portage/make.conf.ftl">
EOF

mkdir -p /etc/portage/package.accept_keywords

<#assign filename = "/etc/portage/package.accept_keywords/libcap">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/etc/portage/package.accept_keywords/libcap.ftl">
EOF


mkdir -p /etc/portage/package.use

<#assign filename = "/etc/portage/package.use/gentoo-sources">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/etc/portage/package.use/gentoo-sources.ftl">
EOF

<#assign filename = "/etc/portage/package.use/genkernel">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/etc/portage/package.use/genkernel.ftl">
EOF

<#assign filename = "/etc/portage/package.accept_keywords/ena-driver">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/etc/portage/package.accept_keywords/ena-driver.ftl">
EOF

<#assign filename = "/etc/portage/package.accept_keywords/eselect-repository">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/etc/portage/package.accept_keywords/eselect-repository.ftl">
EOF


mkdir -p /etc/portage/package.mask

<#assign filename = "/etc/portage/package.mask/gentoo-sources">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/etc/portage/package.mask/gentoo-sources.ftl">
EOF


mkdir -p /etc/sudoers.d

<#assign filename = "/etc/sudoers.d/ec2-user">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/etc/sudoers.d/ec2-user.ftl">
EOF
chmod 440 ${filename}

<#assign filename = "/etc/sudoers.d/_sudo">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/etc/sudoers.d/_sudo.ftl">
EOF
chmod 440 ${filename}

mkdir -p /var/lib/portage

<#assign filename = "/var/lib/portage/world">
echo "${filename}"
cat <<'EOF'>${filename}
<#include "/var/lib/portage/world.ftl">
EOF

env-update
source /etc/profile

/etc/local.d/makeopts.start

#emerge --sync

PORTAGE_NO_SCANELF_CHECK=1 emerge -v1 app-misc/pax-utils

emerge --oneshot sys-apps/portage

<#if architecture == "i386">
    <#assign gentooProfile = "default/linux/x86/17.1">
<#else>
    <#assign gentooProfile = "default/linux/amd64/17.1/no-multilib">
</#if>
eselect profile set ${gentooProfile}

<#if architecture == "i386">
emerge --unmerge sys-apps/module-init-tools
</#if>

emerge mail-mta/ssmtp
emerge --update --deep --with-bdeps=y --newuse @world

cd /usr/src/linux

<#if useRootLabel?? && useRootLabel && rootfstype?starts_with("ext")>

#<#-- /usr/src/linux/.config gets overwritten so naming it .config-ec2 instead -->
<#assign filename = "/usr/src/linux/.config-ec2">
echo "${filename}"
cat <<'__EOF__'>${filename}
<#include "/usr/src/linux/.config.ftl">
__EOF__

genkernel --kernel-config=${filename} --symlink all

<#else>

<#assign filename = "/usr/src/linux/.config">
echo "${filename}"
cat <<'__EOF__'>${filename}
<#include "/usr/src/linux/.config.ftl">
__EOF__

yes "" | make oldconfig
make && make modules_install

<#if architecture == "i386">
    <#assign kernelArch = "x86">
<#else>
    <#assign kernelArch = "x86_64">
</#if>
cp -L arch/${kernelArch}/boot/bzImage /boot/bzImage

</#if>

#emerge app-eselect/eselect-repository app-portage/repoman
#mkdir -p /etc/portage/repos.conf

#eselect repository create localrepo

#mkdir -p /var/db/repos/localrepo/net-misc
#cp -r /var/db/repos/gentoo/net-misc/ena-driver /var/db/repos/localrepo/net-misc

#sed -e \
#"s/dodoc README /dodoc README.rst /" \
#/var/db/repos/localrepo/net-misc/ena-driver/ena-driver-2.2.3-r1.ebuild > /var/db/repos/localrepo/net-misc/ena-driver/ena-driver-2.6.0-r1.ebuild

#sed -e \
#"s/dodoc README /dodoc README.rst /" \
#/var/db/repos/localrepo/net-misc/ena-driver/ena-driver-2.6.1.ebuild > /var/db/repos/localrepo/net-misc/ena-driver/ena-driver-2.6.1-r1.ebuild

#sed -e \
#"s/dodoc README /dodoc README.rst /" \
#/var/db/repos/localrepo/net-misc/ena-driver/ena-driver-2.6.1.ebuild > /var/db/repos/localrepo/net-misc/ena-driver/ena-driver-2.7.1-r1.ebuild

#set $pwd = $PWD
#cd /var/db/repos/localrepo/net-misc/ena-driver
#repoman --digest=y -d full
#cd $pwd

<#if useRootLabel?? && useRootLabel && rootfstype?starts_with("ext") && virtualizationType == "hvm" && architecture == "x86_64">
emerge net-misc/ena-driver

<#assign filename = "/etc/conf.d/modules">
echo "${filename}"
cat <<'EOF'>>${filename}
<#include "/etc/conf.d/modules.ftl">
EOF
</#if>

groupadd sudo
useradd -r -m -s /bin/bash ec2-user

ln -s /etc/init.d/net.lo /etc/init.d/net.eth0

rc-update add net.eth0 default
rc-update add sshd default
rc-update add syslog-ng default
rc-update add fcron default
rc-update add ntpd default
rc-update add lvm boot
rc-update add mdraid boot

<#assign filename = "/etc/default/grub">
echo "${filename}"
cat <<'EOF'>>${filename}
<#include "/etc/default/grub.ftl">
EOF

emerge sys-boot/grub

grub-install ${device}
grub-mkconfig  -o /boot/grub/grub.cfg

